import "./App.css";
import ShoesStore from "./Ex_Shoe_Shop/ShoesStore";

function App() {
  return (
    <div className="container py-4">
      <h2 className="pb-4 text-center">Shoes Shop</h2>
      <ShoesStore />
    </div>
  );
}

export default App;
