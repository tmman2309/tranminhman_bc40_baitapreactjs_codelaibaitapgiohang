import React, { Component } from "react";

export default class Modal extends Component {
  render() {
    let {
      id,
      name,
      alias,
      price,
      description,
      shortDescription,
      quantity,
      image,
    } = this.props.content;

    return (
      <div
        className="modal fade"
        id="exampleModal"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                Product Information
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <p>
                <strong>ID:</strong> {id}
              </p>
              <p>
                <strong>Name:</strong> {name}
              </p>
              <p>
                <strong>Alias:</strong> {alias}
              </p>
              <p>
                <strong>Price:</strong> {price}
              </p>
              <p>
                <strong>Description:</strong> {description}
              </p>
              <p>
                <strong>ShortDescription:</strong> {shortDescription}
              </p>
              <p>
                <strong>Quantity:</strong> {quantity}
              </p>
              <div className="d-flex justify-content-center">
                <img style={{ width: "200px" }} src={image} alt="" />
              </div>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
