import React, { Component } from "react";
import ProductItem from "./ProductItem";

export default class ProductList extends Component {
  render() {
    return (
      <div className="row">
        {this.props.productsData.map((product) => {
          return (
            <ProductItem
              key={product.id}
              item={product}
              handleAddToCart={this.props.handleAddToCart}
              handleModal = {this.props.handleModal}
            />
          );
        })}
      </div>
    );
  }
}
