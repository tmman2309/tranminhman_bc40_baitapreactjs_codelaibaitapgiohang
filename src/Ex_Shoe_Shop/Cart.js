import React, { Component } from "react";

export default class Cart extends Component {
  renderTbody = () => {
    return this.props.cart.map((product) => {
      return (
        <tr>
          <td>{product.id}</td>
          <td>{product.name}</td>
          <td>
            <button
              onClick={() => {
                {
                  this.props.handleSoLuong(product, -1);
                }
              }}
              className="btn btn-danger"
            >
              -
            </button>
            <span className="mx-2">{product.soLuong}</span>
            <button
              onClick={() => {
                {
                  this.props.handleSoLuong(product, 1);
                }
              }}
              className="btn btn-success"
            >
              +
            </button>
          </td>
          <td>${product.price * product.soLuong}.00</td>
          <td>
            <img style={{ width: "80px" }} src={product.image} />
          </td>
          <td>
            <button
              onClick={() => {
                {
                  this.props.handleRemoveFromCart(product);
                }
              }}
              className="btn btn-danger mr-2"
            >
              Xóa
            </button>
            <button
              onClick={() => {
                {
                  this.props.handleModal(product.id);
                }
              }}
              className="btn btn-info"
              data-toggle="modal"
              data-target="#exampleModal"
            >
              Xem
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Quantity</th>
              <th>Price</th>
              <th>Img</th>
              <th>Thao Tác</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
