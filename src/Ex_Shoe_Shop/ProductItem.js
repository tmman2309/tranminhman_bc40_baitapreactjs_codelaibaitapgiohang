import React, { Component } from "react";

export default class ProductItem extends Component {
  render() {
    let { id, image, name, price } = this.props.item;

    return (
      <div className="col-4 p-4">
        <div className="card">
          <a
            onClick={() => {
              {
                this.props.handleModal(id);
              }
            }}
            href="#"
            data-toggle="modal"
            data-target="#exampleModal"
          >
            <img className="card-img-top" src={image} />
          </a>
          <div className="card-body">
            <h4 className="card-title" style={{ fontSize: "18px" }}>
              {name}
            </h4>
            <p className="card-text">${price}.00</p>
            <button
              onClick={() => {
                {
                  this.props.handleAddToCart(this.props.item);
                }
              }}
              className="btn btn-dark"
            >
              Add To Cart
            </button>
          </div>
        </div>
      </div>
    );
  }
}
